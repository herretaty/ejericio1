import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Traductor implements ActionListener {

    private JPanel panel1;
    private JLabel idiomaJLabel;
    public JFrame frame;
    private JTextArea textArea1;
    private JTextArea textArea2;
    private JButton traslateButton;
    private JRadioButton spanishRadioButton;
    private JRadioButton englishRadioButton;
    private JRadioButton frenchRadioButton;
    private JRadioButton frenchRadioButton1;
    private JRadioButton spanishRadioButton1;
    private JRadioButton englishRadioButton1;

    String Español[] = {"Television", "Movil", "Mano", "Ojo", "Cuerpo", "Cama", "Oscuro", "Nieve", "Lluvia",
            "Puerta", "Coche", "Moto"};

    String Ingles[] = {"Televisor", "Mobile", "Hand", "Eye", "Body", "Bed", "Dark", "Snow", "Rain", "Door",
            "Car", "Motorbike"};

    String Frances[] = {"Télévision", "Mobile", "Main", "Oeil", "Corps", "Lit", "Sombre", "Neige", "Pluie",
            "Porte", "Voiture", "Moto"};


    public Traductor() {

        frame = new JFrame("Traductor");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setBounds(300, 300, 400, 600);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        ButtonGroup grupo1 = new ButtonGroup();
        ButtonGroup grupo2 = new ButtonGroup();

        grupo1.add(spanishRadioButton);
        grupo1.add(frenchRadioButton);
        grupo1.add(englishRadioButton);
        grupo2.add(spanishRadioButton1);
        grupo2.add(frenchRadioButton1);
        grupo2.add(englishRadioButton1);

        spanishRadioButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                idiomaJLabel.setText("Español");
            }
        });
        englishRadioButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                idiomaJLabel.setText("Ingles");
            }
        });
        frenchRadioButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                idiomaJLabel.setText("Francés");
            }
        });

        traslateButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                for (int i = 0; i < Ingles.length; i++) {
                    if (spanishRadioButton.isSelected()) {
                        if (textArea2.getText().equalsIgnoreCase(Español[i])) {
                            if (englishRadioButton1.isSelected()) {
                                textArea1.setText(Ingles[i]);
                            } else if (frenchRadioButton1.isSelected()) {
                                textArea1.setText(Frances[i]);
                            } else if (spanishRadioButton1.isSelected()) {
                                textArea1.setText(Español[i]);
                            }
                        }
                    } else if (englishRadioButton.isSelected()) {
                        if (textArea2.getText().equalsIgnoreCase(Ingles[i])) {
                            if (englishRadioButton1.isSelected()) {
                                textArea1.setText(Ingles[i]);
                            } else if (frenchRadioButton1.isSelected()) {
                                textArea1.setText(Frances[i]);
                            } else if (spanishRadioButton1.isSelected()) {
                                textArea1.setText(Español[i]);
                            }
                        }
                    } else if (frenchRadioButton.isSelected()) {
                        if (textArea2.getText().equalsIgnoreCase(Frances[i])) {
                            if (englishRadioButton1.isSelected()) {
                                textArea1.setText(Ingles[i]);
                            } else if (frenchRadioButton1.isSelected()) {
                                textArea1.setText(Frances[i]);
                            } else if (spanishRadioButton1.isSelected()) {
                                textArea1.setText(Español[i]);
                            }
                        }

                    }

                }

            }

        });

    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
